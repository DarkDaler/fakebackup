package main
import (
    "fmt"
    "os"
    "net/http"
    "io/ioutil"
    "strings"
    "time"
    "crypto/tls"
    "log"
    "net"
    "encoding/json"
    "encoding/base32"
    "bytes"
)

//FUNCION QUE REGISTRA A UN NUEVO USER EN EL SISTEMA
func newUserRegistrar(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case "POST":
            body, err := ioutil.ReadAll(r.Body)
            check(err)

            var dat map[string]interface{}
            json.Unmarshal(body, &dat)

            usuario := dat["usuario"].(string)

            pass := dat["password"].(string)
            password, _ := base32.StdEncoding.DecodeString(pass)
            contrasenya := string(password)

            if existeUser(usuario) {
                w.Write([]byte("USER EXISTS"))

            }else {
                //SI NO EXISTE EL USUARIO, LE GENERAMOS UNA CARPETA CON SU HASH, SU TXT CON LA PASS CIFRADA, Y SU ARCHIVO ROUTES VACIO
                os.Mkdir("servidor/" + usuario, 0777)

                fmt.Printf("New user created: "+ usuario + "\n")

                dst, err := os.Create("servidor/" + usuario + "/" + "usr.txt")
                defer dst.Close()
                if err != nil {
                    http.Error(w, err.Error(), http.StatusInternalServerError)
                    return
                }

                writeToFile([]byte(string(contrasenya)), dst.Name())

                dst2, err := os.Create("servidor/" + usuario + "/" + "routes.txt")
                defer dst2.Close()
                if err != nil {
                    http.Error(w, err.Error(), http.StatusInternalServerError)
                    return
                }
            }
            default:
            w.WriteHeader(http.StatusMethodNotAllowed)
    }
}

//FUNCION QUE GUARDA LOS ARCHIVOS EN EL SERVIDOR
func writeOnServer(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case "POST":
        body, err := ioutil.ReadAll(r.Body)
        check(err)

        var dat map[string]interface{}
        json.Unmarshal(body, &dat)
        //COGEMOS LOS DATOS DEL JSON
        nombreCasteado := dat["nombre"].(string)

        usuarioCasteado := dat["usuario"].(string)

        pass := dat["password"].(string)
        password, _ := base32.StdEncoding.DecodeString(pass)
        contrasenyaCastedo := string(password)

        cont := dat["mensaje"].(string)
        mensaje, _ := base32.StdEncoding.DecodeString(cont)
        mensajeCasteado := string(mensaje)

        datosUsr, _ := readFromFile("servidor/"+usuarioCasteado+"/usr.txt")

        //SEGURIDAD
        if string(datosUsr) == contrasenyaCastedo {

            //CREA EL FICHERO Y ESCRIBE LOS DATOS
            dst, err := os.Create("servidor/"+usuarioCasteado+"/"+nombreCasteado)
            defer dst.Close()
            if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
            }

            writeToFile([]byte(mensajeCasteado), dst.Name())

            w.Write([]byte("SUBIDA OK"))
        }else{
            fmt.Printf("Acceso sospechoso detectado \n")
            w.Write([]byte("INVALID PASSWORD"))
        }
    }
}

//FUNCION QUE LOGEA EN EL SERVER
func loginServer(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case "POST":

            body, err := ioutil.ReadAll(r.Body)
            check(err)

            var dat map[string]interface{}
            json.Unmarshal(body, &dat)

            usuario := dat["usuario"].(string)

            pass := dat["password"].(string)
            password, _ := base32.StdEncoding.DecodeString(pass)
            contrasenya := string(password)

            var encontrado bool = false

            datos, _ := readFromFile("servidor/"+usuario+"/usr.txt")
            //SIMPLEMENTE COMPROBAMOS QUE EL USUARIO TIENE LA MISMA CONTRASEÑA(LAS ENVIADA Y LA QUE TENEMOS EN EL SISTEMA VAN CIFRADAS)
            if string(datos) == contrasenya {
                encontrado = true
                fmt.Printf("User "+usuario+" logued \n")
            }
            if encontrado == false {
                w.Write([]byte("INVALID USER"))
            }
    }
}

//FUNCION QUE DEVUELVE EL ARCHIVO ROUTES AL CLIENTE
func uploadRoutes(w http.ResponseWriter, r *http.Request)   {
    switch r.Method {
        case "POST":

        body, err := ioutil.ReadAll(r.Body)
        check(err)

        var dat map[string]interface{}
        json.Unmarshal(body, &dat)

        usuarioCasteado := dat["usuario"].(string)

        pass := dat["password"].(string)
        passw, _ := base32.StdEncoding.DecodeString(pass)
        password := string(passw)

        datosUsr, _ := readFromFile("servidor/"+usuarioCasteado+"/usr.txt")

        //SEGURIDAD
        if string(datosUsr) == password {

            datos, _ := readFromFile("servidor/"+usuarioCasteado+"/routes.txt")

            cadenas := strings.Split(r.RemoteAddr, ":")

            dats := base32.StdEncoding.EncodeToString(datos)

            var jsonStr = []byte(`{"nombre":"routes.txt","mensaje":"`+dats+`"}`)

            request, _ := http.NewRequest("POST", "https://"+cadenas[0]+":8080/writeClient", bytes.NewBuffer(jsonStr))

            cookie, _ := r.Cookie("session")
            request.AddCookie(cookie)

            client := buildClient()
            client.Do(request)
        }else{
            fmt.Printf("Acceso sospechoso detectado \n")
            w.Write([]byte("INVALID PASSWORD"))
        }
    }
}

//FUNCION QUE DEVUELVE LOS ARCHIVOS SOLICITADOS POR EL CLIENTE
func fullBackup(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case "GET":
            body, err := ioutil.ReadAll(r.Body)
            check(err)

            var dat map[string]interface{}
            json.Unmarshal(body, &dat)

            usuarioCasteado := dat["usuario"].(string)

            pass := dat["password"].(string)
            passw, _ := base32.StdEncoding.DecodeString(pass)
            password := string(passw)

            cliente := strings.Split(r.RemoteAddr, ":")

            datos, _ := readFromFile("servidor/"+usuarioCasteado+"/usr.txt")

            if string(datos) == password {
                files, _ := ioutil.ReadDir("servidor/"+usuarioCasteado)

                //RECORREMOS TODOS LOS ARCHIVOS DEL DIRECTORIO Y LOS ENVIAMOS AL CLIENTE, EXCEPTO LOS TXT
                for _, f := range files {
                    if !strings.Contains(f.Name(), ".txt") {
                        datos, _ := readFromFile("servidor/"+usuarioCasteado+"/"+f.Name())

                        dats := base32.StdEncoding.EncodeToString(datos)

                        var jsonStr = []byte(`{"nombre":"`+f.Name() +`","mensaje":"`+dats+`"}`)

                        request, _ := http.NewRequest("POST", "https://"+cliente[0]+":8080/writeClient", bytes.NewBuffer(jsonStr))

                        cookie, _ := r.Cookie("session")
                        request.AddCookie(cookie)

                        client := buildClient()
                        client.Do(request)
                    }
                }
            }else{
                fmt.Printf("Acceso sospechoso detectado \n")
                w.Write([]byte("INVALID PASSWORD"))
            }
    }
}

//FUNCION QUE ELIMINA LOS ARCHIVOS SOLICITADOS POR EL CLIENTE
func deleteFiles(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case "POST":
            body, err := ioutil.ReadAll(r.Body)
            check(err)

            var dat map[string]interface{}
            json.Unmarshal(body, &dat)

            nombreCasteado := dat["usuario"].(string)

            pass := dat["password"].(string)
            passw, _ := base32.StdEncoding.DecodeString(pass)
            password := string(passw)

            archivos := strings.Split(dat["archivos"].(string), "&")

            datos, _ := readFromFile("servidor/"+nombreCasteado+"/usr.txt")

            //SEGURIDAD
            if string(datos) == password {
                //ELIMINA LOS ARCHIVOS MANDADOS
                for i, _ := range archivos {
                    if archivos[i] != "" {
                        os.Remove("servidor/"+nombreCasteado+"/"+archivos[i])
                        fmt.Printf("Archivo del user: "+nombreCasteado+" ->" +archivos[i]+" eliminado \n")
                    }
                }
                w.Write([]byte("DELETED DONE"))

            }else {
                fmt.Printf("Acceso sospechoso detectado \n")
                w.Write([]byte("INVALID PASSWORD"))
            }
    }
}

//FUNCION QUE ELIMINA LOS ARCHIVOS SOLICITADOS POR EL CLIENTE
func sendFiles(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case "POST":
            body, err := ioutil.ReadAll(r.Body)
            check(err)

            var dat map[string]interface{}
            json.Unmarshal(body, &dat)

            nombreCasteado := dat["usuario"].(string)

            pass := dat["password"].(string)
            passw, _ := base32.StdEncoding.DecodeString(pass)
            password := string(passw)

            archivos := strings.Split(dat["archivos"].(string), "&")

            datos, _ := readFromFile("servidor/"+nombreCasteado+"/usr.txt")
            client := strings.Split(r.RemoteAddr, ":")

            //SEGURIDAD
            if string(datos) == password {

                //ENVIA LOS ARCHIVOS PEDIDOS POR EL CLIENTE
                for i, _ := range archivos {
                    if archivos[i] != "" {
                        fileDatos, _ := readFromFile("servidor/"+nombreCasteado+"/"+archivos[i])

                        dats := base32.StdEncoding.EncodeToString(fileDatos)

                        var jsonStr = []byte(`{"nombre":"`+archivos[i]+`","mensaje":"`+dats+`"}`)

                        request, _ := http.NewRequest("POST", "https://"+client[0]+":8080/writeClient", bytes.NewBuffer(jsonStr))

                        cookie, _ := r.Cookie("session")
                        request.AddCookie(cookie)

                        client := buildClient()
                        client.Do(request)
                    }
                }
                w.Write([]byte("SEND DONE"))

            }else {
                fmt.Printf("Acceso sospechoso detectado \n")
                w.Write([]byte("INVALID USER"))
            }
    }
}

//FUNCION QUE SETEA LOS CERTIFICADOS EN LAS PETICIONES DE CLIENTE
func buildClient() *http.Client {

    cert, err := tls.LoadX509KeyPair("./tls/certificado.pem", "./tls/key.pem")
    if err != nil {
        log.Fatalf("Failed to load X509 key pair: %s", err)
    }

    ssl := &tls.Config{
        Certificates: []tls.Certificate{cert},
        InsecureSkipVerify: true,
    }

    client := &http.Client{
        Transport: &http.Transport{
            Dial: func (network, addr string)(net.Conn, error) {
                return net.DialTimeout(network, addr, time.Duration(time.Second*3))
            },
            TLSClientConfig: ssl,
        },
    }

    return client
}

//FUNCION QUE VERIFICA SI UN USER EXISTE
func existeUser(user string) bool {

    if _, err := os.Stat("servidor/"+user); os.IsNotExist(err) {
        return false
    }else {
        return true
    }
}

//FUNCION QUE LEE LOS DATOS DE UN FICHERO
func readFromFile(file string) ([]byte, error) {
    data, err := ioutil.ReadFile(file)
    return data, err
}

//FUNCION QUE GUARDA DATOS EN UN FICHERO
func writeToFile(data []byte, file string) {
    ioutil.WriteFile(file, data, 0777)
}

// función que sirve para comprobar si hay error y salir del programa (panic) en tal caso
func check(e error) {
    if e != nil {
        panic(e)
    }
}

func main() {
    fmt.Println("ESCUCHANDO POR EL PUERTO: 8081")

    os.Mkdir("servidor", 0777)

    http.HandleFunc("/write", writeOnServer)
    http.HandleFunc("/envioRegistro", newUserRegistrar)
    http.HandleFunc("/envioLogin", loginServer)
    http.HandleFunc("/getRoutes", uploadRoutes)
    http.HandleFunc("/fullBackup", fullBackup)
    http.HandleFunc("/deleteFiles", deleteFiles)
    http.HandleFunc("/downloadFiles", sendFiles)

    //Listen on port 8081
    http.ListenAndServeTLS(":8081", "./tls/certificado.pem", "./tls/key.pem", nil)
}

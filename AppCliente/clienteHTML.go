/*
	PREGUNTAR
	-Si un hacker manda una pass incorrecta, ¿logout del sistema?
	-¿Necesidad de crear subida de carpetas?
*/
package main

import (
    "crypto/cipher"
    "crypto/rc4"
    "crypto/sha256"
    "flag"
    "fmt"
    "html/template"
    "io/ioutil"
    "math/rand"
    "net/http"
    "os"
    "strings"
    "time"
    "github.com/gorilla/securecookie"
    "io"
    "strconv"
    "crypto/tls"
    "net"
    "log"
    "sync"
    "encoding/hex"
    "bytes"
    "encoding/base32"
    "encoding/json"
)

var IPserver = getServerIP("127.0.0.1:8081")


type state struct {
    *sync.Mutex // inherits locking methods
}

//SEMAFORO PARA LA SUBIDA DE ARCHIVOS
var State = &state{&sync.Mutex{}}

//COOKIE PARA EL MANEJO DE LA CONTRASEÑA Y EL USER
var cookieHandler = securecookie.New(
securecookie.GenerateRandomKey(64),
securecookie.GenerateRandomKey(32))

//Display the named template
func display(w http.ResponseWriter, tmpl string, data interface{}) {
    template.Must(template.ParseFiles("tmpl/"+ tmpl)).ExecuteTemplate(w, tmpl, data)
}

//FUNCION QUE ENVIA LOS DATOS DE LOGIN Y RENDERIZA
func login(w http.ResponseWriter, r *http.Request) {

    errorPage := struct {
        Tmpl     template.HTML
    }{``}

    switch r.Method {
        case "GET":
        user := getUserName(r) //SI TENEMOS ALGUN USER LOGUEADO YA DE ANTES(COOKIES)
        if user != "" {
            http.Redirect(w, r, "/upload", 302)
        }else {
            display(w, "index.html", nil)
        }
        case "POST":
            r.ParseForm()
            usuarioArray := r.Form["username"]
            contrasenyaArray := r.Form["password"]
            usuario := strings.Join(usuarioArray, "")
            contrasenya := strings.Join(contrasenyaArray, "")

            if contrasenya != "" && usuario != "" {

                //Ciframos la contraseña y generamos el hash del usuario a partir de su user.
                contrasenyaCifrada := encriptar([]byte(contrasenya), string(contrasenya))

                pass := base32.StdEncoding.EncodeToString([]byte(contrasenyaCifrada))

                var jsonStr = []byte(`{"usuario":"`+hashUser(usuario)+`","password":"`+pass+`"}`)

                request, _ := http.NewRequest("POST", "https://"+IPserver+"/envioLogin", bytes.NewBuffer(jsonStr))
                client := buildClient()
                resp, _ := client.Do(request)

                cuerpo, _ := ioutil.ReadAll(resp.Body)
                //Si devuelve error el server, lo mostramos
                if string(cuerpo) == "INVALID USER" {
                    errorPage.Tmpl = `<div class="alert alert-danger col-md-6 col-sm-offset-4">
                                <strong>Usuario o contraseña invalidos</strong>
                                 </div>`
                    display(w, "index.html", errorPage)
                }else {
                    setSession(usuario, contrasenya, w)
                    http.Redirect(w, r, "/upload", 302)
                }

            }else {
                errorPage.Tmpl = `<div class="alert alert-danger col-md-6 col-sm-offset-4">
                                <strong>Usuario o contraseña vacio</strong>
                                 </div>`
                display(w, "index.html", errorPage)
            }
            default:
            w.WriteHeader(http.StatusMethodNotAllowed)
    }
}

//FUNCION QUE ENVIA LOS DATOS DE REGISTRO DESDE EL CLIENTE
func registrar(w http.ResponseWriter, r *http.Request) {

    //Template para mostrar el error en HTML
    errorPage := struct {
        Tmpl     template.HTML
    }{``}

    switch r.Method {
        case "GET":
            user := getUserName(r) //SI TENEMOS ALGUN USER LOGUEADO YA DE ANTES(COOKIES)
            if user != "" {
                http.Redirect(w, r, "/upload", 302)
            }else {
                display(w, "registrar.html", nil)
            }
        case "POST":
            r.ParseForm()
            usuarioArray := r.Form["username"]
            contrasenyaArray := r.Form["pass1"]
            contrasenyaTwoArray := r.Form["pass2"]
            usuario := strings.Join(usuarioArray, "")
            contrasenya := strings.Join(contrasenyaArray, "")
            contrasenyaTwo := strings.Join(contrasenyaTwoArray, "")

            if contrasenya == contrasenyaTwo {

                //Generamos un hash a partir del user, y ciframos la contraseña.
                contrasenyaCifrada := encriptar([]byte(contrasenya), string(contrasenya))

                pass := base32.StdEncoding.EncodeToString([]byte(contrasenyaCifrada))

                var jsonStr = []byte(`{"usuario":"`+hashUser(usuario)+`","password":"`+pass+`"}`)

                request, _ := http.NewRequest("POST", "https://"+IPserver+"/envioRegistro", bytes.NewBuffer(jsonStr))
                client := buildClient()
                resp, _ := client.Do(request)

                cuerpo, _ := ioutil.ReadAll(resp.Body)

                //Si el HASH(User) ya existe, lo mostramos
                if string(cuerpo) == "USER EXISTS" {
                    errorPage.Tmpl = `<div class="alert alert-danger col-sm-offset-4 col-sm-6">
                                <strong>El usuario ya existe</strong>
                                 </div>`
                    display(w, "registrar.html", errorPage)
                }else {
                    setSession(usuario, contrasenya, w)
                    http.Redirect(w, r, "/registrar", 302)
                }

            }else {
                errorPage.Tmpl = `<div class="alert alert-danger col-sm-offset-4 col-sm-6">
                                <strong>No coinciden las contrasenyas</strong>
                                 </div>`
                display(w, "registrar.html", errorPage)
            }
            default:
            w.WriteHeader(http.StatusMethodNotAllowed)
    }
}

//FUNCION QUE DESLOGUEA AL USUARIO DEL SISTEMA
func logout(w http.ResponseWriter, r *http.Request) {
    clearSession(w)
    http.Redirect(w, r, "/", 302)
}

//FUNCIÓN QUE PREPARA EL ARCHIVO PARA LA SUBIDA
func upload(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case "GET":
            user := getUserName(r) //SI TENEMOS ALGUN USER LOGUEADO YA DE ANTES(COOKIES)
            if user != "" {
                getRoutesRequest(w,r)
                archivos := leerRoutes(w,r)//CUANDO CARGAMOS EL PANEL DEL USUARIO, DESCARGAMOS EL ROUTES Y MOSTRAMOS LOS ARCHIVOS
                page := struct {
                    Tmpl     template.HTML
                    User      string
                }{archivos, user}
                display(w, "upload.html", page)
            }else {
                http.Redirect(w, r, "/", 302)
            }

        //POST takes the uploaded file(s) and saves it to disk.
        case "POST":
            State.Lock() //HACEMOS LOCK DE LA FUNCIÓN PARA QUE NO SE ATRANQUE LA SUBIDA
            defer State.Unlock()
            //parse the multipart form in the request
            err := r.ParseMultipartForm(100000)
            if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
            }

            //get a ref to the parsed multipart form
            m := r.MultipartForm

            //get the *fileheaders
            files := m.File["myfiles"]

            getRoutesRequest(w, r)

            for i, _ := range files {

                file, err := files[i].Open()
                defer file.Close()
                if err != nil {
                    http.Error(w, err.Error(), http.StatusInternalServerError)
                    return
                }

                dst, err := ioutil.TempFile(os.TempDir(), files[i].Filename)

                //copy the uploaded file to the destination file
                if _, err := io.Copy(dst, file); err != nil {
                    http.Error(w, err.Error(), http.StatusInternalServerError)
                    return
                }

                datos, _ := readFromFile(dst.Name())

                datosRoutes, _ := readFromFile("cliente/"+getUserName(r)+"/routes.txt")//AÑADIMOS EL ARCHIVO SUBIDO A ROUTES

                if (!strings.Contains(string(datosRoutes), files[i].Filename)) { //SI NO TENEMOS EL ARCHIVO EN EL SERVER
                    claveUtilizada := random(50)//GENERAMOS UNA CLAVE ALEATORIA
                    dstCifrado := encriptar(datos, claveUtilizada) //CIFRAMOS

                    archivoSRV := randInt(1, 100000)//ASIGNAMOS UN NOMBRE AL ARCHIVO EN EL SERVER
                    password := encriptar([]byte(getPassUser(r)), getPassUser(r))

                    request, _ := newfileUploadRequest("https://"+IPserver+"/write", strconv.Itoa(archivoSRV), string(dstCifrado),string(password), hashUser(getUserName(r)))

                    request.Header.Set("X-Custom-Header", "myvalue")
                    request.Header.Set("Content-Type", "application/json")
                    client := buildClient()
                    resp, _ := client.Do(request)

                    cuerpo, _ := ioutil.ReadAll(resp.Body)

                    if string(cuerpo) == "SUBIDA OK" {
                        //SI LA SUBIDA HA SIDO CORRECTA, AÑADIMOS EL ARCHIVO AL ROUTES
                        datosRoutes, _ = readFromFile("cliente/"+getUserName(r)+"/routes.txt")
                        cadena := string(datosRoutes)+(string(files[i].Filename+"/"+strconv.Itoa(archivoSRV)+"/"+claveUtilizada+"\n"))
                        writeToFile([]byte(cadena), "cliente/"+getUserName(r)+"/routes.txt")
                        fmt.Printf("Subida de " + files[i].Filename +" completa \n")
                    }else{
                        fmt.Printf("Error en la subida de " + files[i].Filename +" \n")
                    }


                }else {//SI YA TENEMOS EL ARCHIVO

                    //LA FUNCIÓN ES IGUAL QUE SI NO LO TENEMOS, PERO NO COGEMOS UN NUEVO ID, SINO QUE COGEMOS EL QUE YA TENIAMOS Y LA MISMA PASS
                    cadenas := strings.Split(string(datosRoutes), "\n")

                    numero := ""
                    key := ""
                    for j := range cadenas {
                        if strings.Contains(cadenas[j], files[i].Filename) {
                            var resultado []string = strings.Split(cadenas[j], "/")
                            numero = resultado[1] //ASIGNAMOS MISMO NOMBRE DE SERVIDOR
                            key = resultado[2] //Y MISMA KEY
                            break
                        }
                    }

                    dstCifrado := encriptar(datos, key) //CIFRAMOS CON LA MISMA KEY QUE YA TENIA
                    password := encriptar([]byte(getPassUser(r)), getPassUser(r))

                    request, _ := newfileUploadRequest("https://"+IPserver+"/write", numero, string(dstCifrado),string(password), hashUser(getUserName(r)))

                    request.Header.Set("X-Custom-Header", "myvalue")
                    request.Header.Set("Content-Type", "application/json")
                    client := buildClient()
                    resp, _ := client.Do(request)

                    cuerpo, _ := ioutil.ReadAll(resp.Body)

                    if string(cuerpo) == "SUBIDA OK" {
                        fmt.Printf("Subida de " + files[i].Filename +" completa \n")
                    }else{
                        fmt.Printf("Error en la subida de " + files[i].Filename +" \n")
                    }
                }
                defer os.Remove(dst.Name())
            }
            //FINALMENTE SUBIMOS EL NUEVO ROUTES
            postRoutes(w, r)
            //display success message.
            display(w, "upload.html", "Los archivos se han subido al servidor correctamente")
            default:
            w.WriteHeader(http.StatusMethodNotAllowed)
    }
}

//FUNCION QUE MUESTRA TODOS LOS ARCHIVOS QUE TENEMOS SUBIDOS EN FORMATO HTML
func leerRoutes(w http.ResponseWriter, r *http.Request) template.HTML{

    var templ template.HTML = `<table id="tablica" class="table table-hover">
                        <tr>
                        <th>Nombre</th>
                        <th class="text-center">Marcar para descargar</th>
                        <th></th>
                        </tr>`
    datosRoutes, _ := readFromFile("cliente/"+getUserName(r)+"/routes.txt")

    routesSplit := strings.Split(string(datosRoutes), "\n")

    for i := range routesSplit {
        nombre  := strings.Split(routesSplit[i], "/")

        if nombre[0] != ""{
            var aux template.HTML = `<tr id="`+template.HTML(strconv.Itoa(i))+`" onclick="if(event.target.tagName != 'INPUT') marcar(`+template.HTML(strconv.Itoa(i))+`); enviarDescargas()">
                <td id="f`+template.HTML(strconv.Itoa(i))+`">`+template.HTML(nombre[0])+ `</td>
                <td class="text-center"><input onchange="enviarDescargas()" type="checkbox" id="check`+template.HTML(strconv.Itoa(i))+`" name="check`+template.HTML(strconv.Itoa(i))+`"></td>
                <td><form method="post" action="deleteFiles"><input type="text" hidden="" name="eliminar" id="t`+template.HTML(strconv.Itoa(i))+`" value="`+template.HTML(nombre[0])+`"><input type="submit" value="Eliminar Fichero" class="btn btn-xs button"></form></td>
                </tr>`
            templ = templ + aux
        }
    }

    var aux2 template.HTML = `</table>`
    templ = templ+aux2

    return templ

}

// CREAMOS LA PETICION DE UPLOAD
func newfileUploadRequest(uri string, nombre string, contenido string,password string, user string) (*http.Request, error) {

    pass := base32.StdEncoding.EncodeToString([]byte(password))
    cont := base32.StdEncoding.EncodeToString([]byte(contenido))

    var jsonStr = []byte(`{"nombre":"`+nombre+`","usuario":"`+user+`","password":"`+pass+`","mensaje":"`+cont+`"}`)

    return http.NewRequest("POST", uri, bytes.NewBuffer(jsonStr))
}

//FUNCION QUE ELIMINA LOS ARCHIVOS SELECCIONADOS
func deleteFiles(w http.ResponseWriter, r *http.Request){
    switch r.Method {
        case "GET":
            http.Redirect(w, r, "/upload", 302)
        case "POST":
            r.ParseForm()
            filesArray := r.Form["eliminar"]
            files := strings.Join(filesArray, "")

            if files != "" {
                password := encriptar([]byte(getPassUser(r)), getPassUser(r))

                datosRoutes, _ := readFromFile("cliente/"+getUserName(r)+"/routes.txt")
                bodySplit := strings.Split(files, "&")
                routesSplit := strings.Split(string(datosRoutes), "\n")

                var archivos string = ""
                routesFinal := ""

                //VEMOS LOS ARCHIVOS QUE TENEMOS EN ROUTES,COGEMOS LOS ID DE LOS QUE QUEREMOS ELIMINAR,
                //Y ELIMINAMOS LA LINEA DEL ARCHIVO CORRESPONDIENTE
                for i, _ := range bodySplit {
                    for j, _ := range routesSplit {
                        if (strings.Contains(routesSplit[j], bodySplit[i])) {
                            id := strings.Split(routesSplit[j], "/")
                            archivos = archivos +"&" +  id[1]
                            routesSplit[j] = "" //BORRAMOS LA ENTRADA EN ROUTES
                            break
                        }
                    }
                }

                pass := base32.StdEncoding.EncodeToString([]byte(string(password)))

                var jsonStr = []byte(`{"usuario":"`+hashUser(getUserName(r))+`","password":"`+pass+`","archivos":"`+archivos+`"}`)

                request, _ := http.NewRequest("POST", "https://"+IPserver+"/deleteFiles", bytes.NewBuffer(jsonStr))
                client := buildClient()
                resp, _ := client.Do(request)

                cuerpo, _ := ioutil.ReadAll(resp.Body)

                if string(cuerpo) == "DELETED DONE" {
                    //GUARDAMOS EL NUEVO ARCHIVO DE ROUTES
                    for i, _ := range routesSplit {
                        if routesSplit[i] != "" {
                            routesFinal = routesFinal + routesSplit[i] + "\n"
                        }
                    }
                    writeToFile([]byte(routesFinal), "cliente/"+getUserName(r)+"/routes.txt")
                    postRoutes(w, r)
                    http.Redirect(w, r, "/upload", 302)
                }else {

                    http.Redirect(w, r, "/upload", 302)
                }
            }else {

                http.Redirect(w, r, "/upload", 302)
            }
    }
}

//FUNCION QUE DESCARGA LOS ARCHIVOS SELECCIONADOS
func downloadFiles(w http.ResponseWriter, r *http.Request){
    switch r.Method {
        case "GET":
            http.Redirect(w, r, "/upload", 302)
        case "POST":
            r.ParseForm()
            filesArray := r.Form["descargar"]
            files := strings.Join(filesArray, "")
            if files != "" {

                password := encriptar([]byte(getPassUser(r)), getPassUser(r))

                datosRoutes, _ := readFromFile("cliente/"+getUserName(r)+"/routes.txt")
                bodySplit := strings.Split(files, "&")
                routesSplit := strings.Split(string(datosRoutes), "\n")

                var archivos string = ""

                //GUARDAMOS LOS ID DE LOS ARCHIVOS QUE QUEREMOS DESCARGAR
                for i, _ := range bodySplit {
                    for j, _ := range routesSplit {
                        if (strings.Contains(routesSplit[j], bodySplit[i])) {
                            id := strings.Split(routesSplit[j], "/")
                            archivos = archivos +"&" +  id[1]
                            break
                        }
                    }
                }

                pass := base32.StdEncoding.EncodeToString([]byte(string(password)))

                var jsonStr = []byte(`{"usuario":"`+hashUser(getUserName(r))+`","password":"`+pass+`","archivos":"`+archivos+`"}`)

                request, _ := http.NewRequest("POST", "https://"+IPserver+"/downloadFiles",  bytes.NewBuffer(jsonStr))

                cookie, _ := r.Cookie("session")
                request.AddCookie(cookie)

                client := buildClient()
                resp, _ := client.Do(request)

                cuerpo, _ := ioutil.ReadAll(resp.Body)

                if string(cuerpo) == "DOWNLOAD DONE" {

                    http.Redirect(w, r, "/upload", 302)
                }else {

                    http.Redirect(w, r, "/upload", 302)
                }
            }else {

                http.Redirect(w, r, "/upload", 302)
            }
    }
}

//FUNCION QUE DESCARGA EL ARCHIVO ROUTES PARA AÑADIR LOS NUEVOS ARCHIVOS SUBIDOS
func getRoutesRequest(w http.ResponseWriter, r *http.Request) {

    password := encriptar([]byte(getPassUser(r)), getPassUser(r))

    pass := base32.StdEncoding.EncodeToString(password)

    var jsonStr = []byte(`{"usuario":"`+hashUser(getUserName(r))+`","password":"`+pass+`"}`)

    request, _ := http.NewRequest("POST", "https://"+IPserver+"/getRoutes", bytes.NewBuffer(jsonStr))

    cookie, _ := r.Cookie("session")
    request.AddCookie(cookie)

    client := buildClient()
    client.Do(request)
}

//FUNCION QUE ENVIA EL ROUTES AL SERVER
func postRoutes(w http.ResponseWriter, r *http.Request) {

    datosRoutes, _ := readFromFile("cliente/"+getUserName(r)+"/routes.txt")

    datosRoutesEncriptado := encriptar(datosRoutes, getPassUser(r))
    password := encriptar([]byte(getPassUser(r)), getPassUser(r))

    request, _ := newfileUploadRequest("https://"+IPserver+"/write", "routes.txt", string(datosRoutesEncriptado),string(password), hashUser(getUserName(r)))
    client := buildClient()
    client.Do(request)
}

//PETICION DE FULL BACKUP AL SERVER
func downloadFullBackup(w http.ResponseWriter, r *http.Request) {

    getRoutesRequest(w, r)

    password := encriptar([]byte(getPassUser(r)), getPassUser(r))

    pass := base32.StdEncoding.EncodeToString(password)

    var jsonStr = []byte(`{"usuario":"`+hashUser(getUserName(r))+`","password":"`+pass+`"}`)

    request, _ := http.NewRequest("GET", "https://"+IPserver+"/fullBackup", bytes.NewBuffer(jsonStr))

    cookie, _ := r.Cookie("session")
    request.AddCookie(cookie)

    client := buildClient()
    client.Do(request)

    http.Redirect(w, r, "/upload", 302)
}

//FUNCION QUE GUARDA LOS ARCHIVOS EN EL CLIENTE
func writeOnClient(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case "GET":
            http.Redirect(w, r, "/upload", 302)

        case "POST":
            body, err := ioutil.ReadAll(r.Body)
            check(err)

            var dat map[string]interface{}
            json.Unmarshal(body, &dat)

            nombreCasteado := dat["nombre"].(string)

            cont := dat["mensaje"].(string)
            mensaje, _ := base32.StdEncoding.DecodeString(cont)
            mensajeCasteado := string(mensaje)

            os.Mkdir("cliente/"+getUserName(r), 0777)

            if strings.Contains(nombreCasteado, "routes.txt") { //SI ES EL ROUTES

                datosDesencriptados := encriptar([]byte(mensajeCasteado),getPassUser(r))

                writeToFile(datosDesencriptados, "cliente/"+getUserName(r)+"/"+nombreCasteado)

            }else { //SI ES OTRO ARCHIVO
                routes, _ := readFromFile("cliente/"+getUserName(r)+"/routes.txt")
                routesSplit := strings.Split(string(routes), "\n")

                for i, _ := range routesSplit {
                    nombre := strings.Split(routesSplit[i], "/")

                    if strings.Contains(nombre[1], nombreCasteado) {

                        datos := encriptar([]byte(mensajeCasteado), nombre[2])
                        writeToFile(datos, "cliente/"+getUserName(r)+"/"+nombre[0])
                        break
                    }
                }
            }
    }
}

// función que sirve para comprobar si hay error y salir del programa (panic) en tal caso
func check(e error) {
    if e != nil {
        panic(e)
    }
}

//FUNCION QUE ENCRIPTA UNA CADENA CON UNA CLAVE ALEATORIA EN RC4
func encriptar(cadena []byte, clave string) []byte {
    // estas variables son punteros
    pK := clave

    // hay que llamar a flag.Parse para que compruebe los flags y asigne valores
    flag.Parse()

    // si no hay clave (parámetro obligatorio) imprimimos el mensaje de uso y salimos
    if pK == "" {
        flag.PrintDefaults()
        os.Exit(1)
    }

    // Obtenemos el hash (256bit) de la clave introducida por el usuario
    // De esta forma podemos usar cualquier cadena como clave
    h := sha256.New()
    h.Reset()
    _, err := h.Write([]byte(pK))
    check(err)
    key := h.Sum(nil)

    /*
       Para cifrar y descifrar utilizamos el interfaz cipher.Stream que luego utilizamos en cipher.StreamWriter o
       cipher.StreamReader según necesitemos cifrar o descifrar. Este enfoque nos permite trabajar con todos los
       cifradores del mismo modo, tanto si son de bloque (en modo CTR) como en flujo.
    */

    // definimos la variable de tipo cipher.Stream
    var S cipher.Stream

    //Cifrador RC4
    c, err := rc4.NewCipher(key) // usamos toda la clave
    check(err)
    S = c

    // definimos un lector y un escritor que asignaremos para copiar al final del lector al escritor

    resultado := make([]byte, len(cadena)) //Convertimos la cadena a bytes

    S.XORKeyStream(resultado, []byte(cadena))

    return resultado
}

//FUNCION QUE LEE LOS DATOS DE UN FICHERO
func readFromFile(file string) ([]byte, error) {
    data, err := ioutil.ReadFile(file)
    return data, err
}

//FUNCION QUE GUARDA DATOS EN UN FICHERO
func writeToFile(data []byte, file string) error{
    return ioutil.WriteFile(file, data, 0777)
}

//FUNCION QUE GENERA UNA CLAVE ALEATORIA PARA ENCRIPTAR
func random(longitud int) string {
    var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
    r := rand.New(rand.NewSource(int64(time.Now().Nanosecond())))

    b := make([]rune, longitud)
    for i := range b {
        b[i] = letters[r.Intn(len(letters))]
    }
    return string(b)
}

//FUNCION QUE GENERA UN NUMERO ALEATORIO ENTRE EL MINIMO Y EL MAXIMO
func randInt(min int, max int) int {
    r := rand.New(rand.NewSource(int64(time.Now().Nanosecond())))
    return min + r.Intn(max-min)
}

//FUNCION QUE SETEA LOS CERTIFICADOS EN LAS PETICIONES DE CLIENTE
func buildClient() *http.Client {

    cert, err := tls.LoadX509KeyPair("./tls/certificado.pem", "./tls/key.pem")
    if err != nil {
        log.Fatalf("Failed to load X509 key pair: %s", err)
    }

    ssl := &tls.Config{
        Certificates: []tls.Certificate{cert},
        InsecureSkipVerify: true,
    }

    client := &http.Client{
        Transport: &http.Transport{
            Dial: func (network, addr string)(net.Conn, error) {
                return net.DialTimeout(network, addr, time.Duration(time.Second*3))
            },
            TLSClientConfig: ssl,
        },
    }

    return client
}

//FUNCION QUE SETEA EL USUARIO QUE SE HA LOGUEADO
func setSession(userName string, pass string, response http.ResponseWriter) {
    value := map[string]string{
        "name": userName,
        "pass": pass,
    }
    if encoded, err := cookieHandler.Encode("session", value); err == nil {
        cookie := &http.Cookie{
            Name:  "session",
            Value: encoded,
            Path:  "/",
        }
        cookie.MaxAge = 3600
        http.SetCookie(response, cookie)
    }
}

//FUNCION QUE RECOJE LA CONTRASEÑA DEL USUARIO LOOGUEADO EN EL MOMENTO
func getPassUser(request *http.Request) (passUser string) {
    if cookie, err := request.Cookie("session"); err == nil {
        cookieValue := make(map[string]string)
        if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
            passUser = cookieValue["pass"]
        }
    }
    return passUser
}

//FUNCION QUE RECOJE EL USUARIO LOGEADO EN EL MOMENTO
func getUserName(request *http.Request) (userName string) {
    if cookie, err := request.Cookie("session"); err == nil {
        cookieValue := make(map[string]string)
        if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
            userName = cookieValue["name"]
        }
    }
    return userName
}

//FUNCION QUE LIMPIA EL USUARIO CUANDO SE DESLOGUEA
func clearSession(response http.ResponseWriter) {
    cookie := &http.Cookie{
        Name:   "session",
        Value:  "",
        Path:   "/",
        MaxAge: -1,
    }
    http.SetCookie(response, cookie)
}

//DEVUELVE LA IP DEL SERVER
func getServerIP(server string) string {
    return server
}

//FUNCION QUE DEVUELVE EL HASH DEL USER
func hashUser(user string) string{

    h := sha256.New()
    h.Reset()
    _, err := h.Write([]byte(user))
    check(err)
    key := h.Sum(nil)

    return hex.EncodeToString(key)
}

func main() {

    fmt.Println("ESCUCHANDO POR EL PUERTO: 8080")

    os.Mkdir("cliente", 0777)

    http.HandleFunc("/", login)
    http.HandleFunc("/logout", logout)
    http.HandleFunc("/registrar", registrar)
    http.HandleFunc("/upload", upload)
    http.HandleFunc("/writeClient", writeOnClient)
    http.HandleFunc("/deleteFiles", deleteFiles)
    http.HandleFunc("/downFullBackup", downloadFullBackup)
    http.HandleFunc("/downFiles", downloadFiles)

    //static file handler.
    http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))

    //Listen on port 8080
    http.ListenAndServeTLS(":8080", "./tls/certificado.pem", "./tls/key.pem", nil)

}
